# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Rancher System. The API that was used to build the adapter for Rancher is usually available in the report directory of this adapter. The adapter utilizes the Rancher API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Rancher adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Rancher Cloud Management. With this adapter you have the ability to perform operations such as:

- Configure and Manage Rancher Connectors. 
- Get Segment ID
- Create IPSec Connector
- Create Internet Connector
- Create AWS VPC Connector
- Create Azure VNet Connector

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
