
## 0.3.4 [10-14-2024]

* Changes made at 2024.10.14_19:27PM

See merge request itentialopensource/adapters/adapter-rancher!10

---

## 0.3.3 [09-15-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-rancher!8

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:27PM

See merge request itentialopensource/adapters/adapter-rancher!7

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:25PM

See merge request itentialopensource/adapters/adapter-rancher!6

---

## 0.3.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-rancher!5

---

## 0.2.3 [03-28-2024]

* Changes made at 2024.03.28_13:06PM

See merge request itentialopensource/adapters/cloud/adapter-rancher!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_15:25PM

See merge request itentialopensource/adapters/cloud/adapter-rancher!3

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:35AM

See merge request itentialopensource/adapters/cloud/adapter-rancher!2

---

## 0.2.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-rancher!1

---

## 0.1.4 [06-21-2023]

* Bug fixes and performance improvements

See commit 44edbf8

---

## 0.1.3 [06-19-2023]

* fixed spec

See merge request itentialopensource/adapters/cloud/adapter-rancher!1

---

## 0.1.2 [06-19-2023]

* fixed spec
