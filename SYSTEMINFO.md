# Rancher

Vendor: Rancher
Homepage: https://www.rancher.com/

Product: Rancher
Product Page: https://www.rancher.com/

## Introduction
We classify Rancher into the Cloud domain as Rancher provide Cloud Services.

"Rancher is a complete software stack for teams adopting containers. It addresses the operational and security challenges of managing multiple Kubernetes clusters, while providing DevOps teams with integrated tools for running containerized workloads."

## Why Integrate
The Rancher adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Rancher Cloud Management. With this adapter you have the ability to perform operations such as:

- Configure and Manage Rancher Connectors. 
- Get Segment ID
- Create IPSec Connector
- Create Internet Connector
- Create AWS VPC Connector
- Create Azure VNet Connector

## Additional Product Documentation

The [API documents for Rancher](https://ranchermanager.docs.rancher.com/api/api-reference)
The [API usage for Rancher](https://rancher.com/docs/rancher/v1.6/en/api/v2-beta/)
